-- Insert Staff (not possible yet to define department given that the
-- respective table is empty!)
INSERT INTO people
    ( name, email, code )
  VALUES
    ( 'Professor António Fernando',
      'paf@fe.up.pt',
      '174398' );
INSERT INTO staff
    ( acronym, code )
  VALUES
    ( 'PAF',
      '174398' );

INSERT INTO people
    ( name, email, code )
  VALUES
    ( 'Professor Maria Ana',
      'pma@fe.up.pt',
      '291845' );
INSERT INTO staff
    ( acronym, code )
  VALUES
    ( 'PMA',
      '291845' );

INSERT INTO people
    ( name, email, code )
  VALUES
    ( 'Professor Filipe Silva',
      'pfs@fe.up.pt',
      '302997' );
INSERT INTO staff
    ( acronym, code )
  VALUES
    ( 'PFS',
      '302997' );

INSERT INTO people
    ( name, email, code )
  VALUES
    ( 'Professor Rafael Norberto',
      'prn@fe.up.pt',
      '200488' );
INSERT INTO staff
    ( acronym, code )
  VALUES
    ( 'PRN',
      '200488' );

INSERT INTO people
    ( name, email, code )
  VALUES
    ( 'Head Master José Silva',
      'hmjs@fe.up.pt',
      '100299' );
INSERT INTO staff
    ( acronym, code )
  VALUES
    ( 'HMJS',
      '100299' );

INSERT INTO people
    ( name, email, code )
  VALUES
    ( 'Professor Director João Santos',
      'pdjs@fe.up.pt',
      '102931' );
INSERT INTO staff
    ( acronym, code )
  VALUES
    ( 'PDJS',
      '102931' );

INSERT INTO people
    ( name, email, code )
  VALUES
    ( 'Professor Director Elsa Ramos',
      'pder@fe.up.pt',
      '123098' );
INSERT INTO staff
    ( acronym, code )
  VALUES
    ( 'PDER',
      '123098' );

-- Insert Department DEEC
INSERT INTO department
    ( name, acronym, head )
  VALUES
    ( 'Department of Electrical and Computer Engineering',
      'DEEC',
      '100299' );

-- Update department membership
UPDATE staff
  SET member_department = 'DEEC'
  WHERE code = '174398';  -- only updates membership of PAF (174398)

UPDATE staff
  SET member_department = 'DEEC'; -- applies to all rows in staff

-- Insert Students
INSERT INTO people
    ( name, email, code )
  VALUES
    ( 'Student Hugo Silva',
      'up201512345@fe.up.pt',
      'up201512345' );
INSERT INTO student ( code ) VALUES ( 'up201512345' );

-- Add courses (necessary to have courses before associating enrollment)
INSERT INTO course
    ( name, acronym, director )
  VALUES
    ( 'Master in Electrical and Computers Engineering',
      'MIEEC',
      '102931' );

INSERT INTO course
    ( name, acronym, director )
  VALUES
    ( 'Doctoral Program in Electrical and Computer Engineering',
      'PDEEC',
      '123098' );

-- Insert enrollments
INSERT INTO enrolled
    ( current_year, status, student_code, course_acronym )
  VALUES
    ( 5,
      'Concluded',
      'up201512345',
      'MIEEC' );

INSERT INTO enrolled
    ( current_year, status, student_code, course_acronym )
  VALUES
    ( 2,
      'Ongoing',
      'up201512345',
      'PDEEC' );
