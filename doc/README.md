# Documentation of PostgreSQLExample

## UML Class Diagram

![UML Class Diagram of the PostgreSQLExample](img/postgresqlexample.drawio.png)

## Relational Model

**Verbose version**

- People (name: string [NN], email: string [UK,NN], code: string [PK])
- Staff (acronym: string [UK,NN], #code: string &rarr; People(code) [PK],
  #member_department: string &rarr; Department(acronym))
- Student (#code: string &rarr; People(code) [PK])
- Department (name: string [NN], acronym: string [PK],
  #head: string &rarr; Staff(code))
- Course (name: string [NN], acronym: string [PK],
  #director: string &rarr; Staff(code))
- Enrolled (id: integer [PK], current_year: integer [NN], status: string [NN],
  #student_code: string &rarr; Student(code) [NN],
  #course_acronym: string &rarr; Course(acronym) [NN])

**Summary version**

- People (name [NN], email [UK,NN], code [PK])
- Staff (acronym [UK,NN], #code &rarr; People [PK], #acronym &rarr; Department)
- Student (#code &rarr; People [PK])
- Department (name [NN], acronym [PK], #code &rarr; Staff)
- Course (name [NN], acronym [PK], #code &rarr; Staff)
- Enrolled (id [PK], current_year [NN], status [NN], #code &rarr; Student [NN],
  #acronym &rarr; Course [NN])

**Additional observations**

- PK: primary key
- UK: unique key
- NN: not null constraint

## Database

All SQL code used for the PostgreSQLExample project is available in the
[sql](/sql/) folder.

### Schema

1. Open SQL code execution in the phpPgAdmin interface (Right-corner > SQL)
2. Select Database `pswa0409`
3. Execute the following SQL code:
   ```sql
   CREATE SCHEMA psw;
   SHOW search_path;
   SET search_path TO psw;
   SHOW search_path;
   ```

_Note:_ you should always create a schema for different projects. Do not forget
to set the schema before executing any query in your application.

### Tables Creation

1. Open SQL code execution in the phpPgAdmin interface (Right-corner > SQL)
2. Select Database `pswa0409`
3. Change Schema search path for `psw`
3. Execute the following SQL code (only one example shown here; for more, go to
   the [sql](/sql/) folder):
   ```sql
   CREATE TABLE people (
     name varchar(50) NOT NULL,
     email varchar(50) NOT NULL UNIQUE,
     code varchar(11) PRIMARY KEY -- upXXXXXXXXX ~ 11 characters
   );
   ```

### Populate Database

1. Open SQL code execution in the phpPgAdmin interface (Right-corner > SQL)
2. Select Database `pswa0409`
3. Change Schema search path for `psw`
4. Execute `INSERT INTO <table> (<...attributes...>) VALUES (<...values...>)`
  (only one example shown here; for more, go to the [sql](/sql/) folder):
  ```sql
   INSERT INTO people
       ( name, email, code )
     VALUES
       ( 'Ricardo Barbosa Sousa',
         'rbs@fe.up.pt',
         '668475' );
  ```

### Reset Database

1. Open SQL code execution in the phpPgAdmin interface (Right-corner > SQL)
2. Select Database `pswa0409`
3. Change Schema search path for `psw`
4. Execute [`reset.sql`](/sql/reset.sql)
5. Execute [`populate.sql`](/sql/populate.sql) (if you want to repopulate your
   database)
