#pragma once

#include <iostream>

namespace logic {

class People {
 public:
  std::string name;
  std::string email;
  std::string code;

 public:
  explicit People(const std::string name, const std::string email,
      const std::string code);
};

} // namespace logic
