#pragma once

#include <iostream>
#include <exception>

#ifdef __unix__
#include <postgresql/libpq-fe.h>
#else
#include <libpq-fe.h>
#endif

namespace db {

const std::string kDbHostIP = "10.227.240.130";
const std::string kDbName = "pswa0409";
const std::string kDbUsername = "pswa0409";
const std::string kDbPassword = "ylSLRIPz";

class Connection {
 private:
  PGconn* dbconn_ = nullptr;
  std::string dbconn_str_;

  std::string dbname_;
  std::string host_;
  std::string user_;
  std::string password_;

 public:
  explicit Connection(const std::string dbname, const std::string host,
      const std::string user, std::string password);
  ~Connection();

  void connect();
  void close();

  bool isGood();

  PGconn* getPGconn();
  std::string& getConnectionStr();
};

} // namespace db
