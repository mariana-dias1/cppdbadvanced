#pragma once

#include <iostream>
#include <exception>

#include "db/Connection.h"

namespace db {

class Query {
 private:
  Connection& dbconn_;

 public:
  explicit Query(Connection& dbconn);

  void setSchema(const std::string schema);
  std::string showSearchPath();

 protected:
  PGresult* execute(const std::string query);
};

} // namespace db
