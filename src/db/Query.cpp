#include "db/Query.h"

namespace db {

Query::Query(Connection& dbconn) : dbconn_(dbconn) { }

void Query::setSchema(const std::string schema) {
  PGresult* result = execute("SET search_path TO " + schema + ";");
  PQclear(result);
}

std::string Query::showSearchPath() {
  PGresult* result = execute("SHOW search_path;");
  std::string search_path = PQgetvalue(result, 0, 0);
  PQclear(result);

  return search_path;
}

PGresult* Query::execute(const std::string query) {
  if (dbconn_.isGood()) {
    PGresult* result = PQexec(dbconn_.getPGconn(), query.c_str());

    if (PQresultStatus(result) == PGRES_FATAL_ERROR) {
        throw std::runtime_error(
            "[QUERY] Error occurred when executing ' " + query + "'\n" +
            std::string(PQerrorMessage(dbconn_.getPGconn())));
    } else {
      return result;
    }

  } else {
    throw std::runtime_error(
        "[CONNECTION] Error when connecting to the database " +
        dbconn_.getConnectionStr());
  }
}

} // namespace db
