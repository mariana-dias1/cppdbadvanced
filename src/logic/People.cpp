#include "logic/People.h"

namespace logic {

People::People(const std::string name, const std::string email,
               const std::string code) {
  this->name = name;
  this->email = email;
  this->code = code;
}

} // namespace logic
